// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// toml-matic - a TOML supported configuration manager
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"fmt"
	"os"
	"testing"
	"time"

	"gitlab.com/quoeamaster/tomlmatic/pkg/config"
)

func Test_tomlParseDefaultFilepath(t *testing.T) {
	t.Logf("\n** [Test_tomlParseDefaultFilepath] {%v} **\n", time.Now().Format(time.RFC3339Nano))
	s, e := config.CreateManager(config.ModelTomlOnly, "", "")
	if nil != e {
		t.Fatalf("1. failed to create a tomlmanager, %v", e)
	}
	if e := s.Parse(); nil != e {
		t.Fatalf("2. failed to parse the file contents, %v", e)
	}
	// access values
	type test struct {
		key         string
		value       interface{}
		stringValue string
		intValue    int64
		floatValue  float64
		boolValue   bool
		// 1 = interface, 2 = string, 3 = int, 4 = float32, 5 = bool
		expectedValueType int
		fail              bool
	}

	m1 := make(map[string]interface{})
	m1["id"] = 12

	tests := []test{
		{key: "purpose", expectedValueType: 2, stringValue: "testing the toml manager~"},
		{key: "server.ip", expectedValueType: 2, stringValue: "127.0.0.1"},
		{key: "server.port", expectedValueType: 3, intValue: 1999},
		{key: "server.variance", expectedValueType: 4, floatValue: 1.93},
		{key: "server.paths.log", expectedValueType: 2, stringValue: "/var/log/server"},
		{key: "server.paths.debug", expectedValueType: 2, stringValue: "/var/log/server/debug"},
		{key: "debug", expectedValueType: 5, boolValue: true},
		{key: "obj", expectedValueType: 1, value: m1},

		// wrong type...
		{key: "server.port", fail: true, expectedValueType: 2, stringValue: "1999"},
		// no such key
		{key: "unknown", fail: true},
		// no such key
		{key: "unknown.what", fail: true},
	}

	for i, tt := range tests {
		t.Logf("3. [%v] testing %v", i, tt)
		switch tt.expectedValueType {
		case 1:
			// interface{}
			v, e := s.Value(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3a. [%v] failed to Value, reason: %v", i, e)
				}
			}
			m := v.(map[string]interface{})
			if fmt.Sprintf("%v", m) != fmt.Sprintf("%v", tt.value) {
				if !tt.fail {
					t.Fatalf("3a. [%v] failed to MATCH Value, reason: %v vs %v", i, fmt.Sprintf("%v", m), fmt.Sprintf("%v", tt.value))
				}
			}
		case 2:
			// string
			v, e := s.StringValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3b. [%v] failed to StringValue, reason: %v", i, e)
				}
			}
			if v != tt.stringValue {
				if !tt.fail {
					t.Fatalf("3b. [%v] failed to MATCH StringValue, reason: [%v] vs %v", i, v, tt.stringValue)
				}
			}
		case 3:
			// int
			v, e := s.IntValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3c. [%v] failed to intValue, reason: %v", i, e)
				}
			}
			if v != tt.intValue {
				if !tt.fail {
					t.Fatalf("3c. [%v] failed to MATCH intvalue, reason: [%v] vs %v", i, v, tt.intValue)
				}
			}
		case 4:
			// float32
			v, e := s.FloatValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3d. [%v] failed to floatValue, reason: %v", i, e)
				}
			}
			if v != tt.floatValue {
				if !tt.fail {
					t.Fatalf("3d. [%v] failed to MATCH floatvalue, reason: [%v] vs %v", i, v, tt.floatValue)
				}
			}
		case 5:
			// bool
			v, e := s.BoolValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3e. [%v] failed to boolValue, reason: %v", i, e)
				}
			}
			if v != tt.boolValue {
				if !tt.fail {
					t.Fatalf("3e. [%v] failed to MATCH boolvalue, reason: [%v] vs %v", i, v, tt.boolValue)
				}
			}
		}
	}

	t.Logf("\n** [Test_tomlParseDefaultFilepath][done] {%v} **\n", time.Now().Format(time.RFC3339Nano))
}

func Test_tomlParseEnvPath(t *testing.T) {
	t.Logf("\n** [Test_tomlParseEnvPath] {%v} **\n", time.Now().Format(time.RFC3339Nano))
	os.Setenv("conf-location", "./server.conf.env")
	defer os.Unsetenv("conf-location")

	s, e := config.CreateManager(config.ModelTomlOnly, "conf-location", "")
	if nil != e {
		t.Fatalf("1. failed to create a tomlmanager, %v", e)
	}
	if e := s.Parse(); nil != e {
		t.Fatalf("2. failed to parse the file contents, %v", e)
	}
	// access values
	type test struct {
		key         string
		value       interface{}
		stringValue string
		intValue    int64
		floatValue  float64
		boolValue   bool
		// 1 = interface, 2 = string, 3 = int, 4 = float32
		expectedValueType int
		fail              bool
	}

	m1 := make(map[string]interface{})
	m1["id"] = 99

	tests := []test{
		{key: "purpose", expectedValueType: 2, stringValue: "testing the toml manager~"},
		{key: "server.ip", expectedValueType: 2, stringValue: "192.168.0.1"},
		{key: "server.port", expectedValueType: 3, intValue: 23891},
		{key: "server.variance", expectedValueType: 4, floatValue: 3.14},
		{key: "server.paths.log", expectedValueType: 2, stringValue: "/var/log/server"},
		{key: "server.paths.debug", expectedValueType: 2, stringValue: "/var/log/server/debug"},
		{key: "obj", expectedValueType: 1, value: m1},

		// wrong type...
		{key: "server.port", fail: true, expectedValueType: 2, stringValue: "1999"},
		// no such key
		{key: "unknown", fail: true},
		// no such key
		{key: "unknown.what", fail: true},
	}

	for i, tt := range tests {
		t.Logf("3. [%v] testing %v", i, tt)
		switch tt.expectedValueType {
		case 1:
			// interface{}
			v, e := s.Value(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3a. [%v] failed to Value, reason: %v", i, e)
				}
			}
			m := v.(map[string]interface{})
			if fmt.Sprintf("%v", m) != fmt.Sprintf("%v", tt.value) {
				if !tt.fail {
					t.Fatalf("3a. [%v] failed to MATCH Value, reason: %v vs %v", i, fmt.Sprintf("%v", m), fmt.Sprintf("%v", tt.value))
				}
			}
		case 2:
			// string
			v, e := s.StringValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3b. [%v] failed to StringValue, reason: %v", i, e)
				}
			}
			if v != tt.stringValue {
				if !tt.fail {
					t.Fatalf("3b. [%v] failed to MATCH StringValue, reason: [%v] vs %v", i, v, tt.stringValue)
				}
			}
		case 3:
			// int
			v, e := s.IntValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3c. [%v] failed to intValue, reason: %v", i, e)
				}
			}
			if v != tt.intValue {
				if !tt.fail {
					t.Fatalf("3c. [%v] failed to MATCH intvalue, reason: [%v] vs %v", i, v, tt.intValue)
				}
			}
		case 4:
			// float32
			v, e := s.FloatValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3d. [%v] failed to floatValue, reason: %v", i, e)
				}
			}
			if v != tt.floatValue {
				if !tt.fail {
					t.Fatalf("3d. [%v] failed to MATCH floatvalue, reason: [%v] vs %v", i, v, tt.floatValue)
				}
			}
		case 5:
			// bool
			v, e := s.BoolValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3e. [%v] failed to boolValue, reason: %v", i, e)
				}
			}
			if v != tt.boolValue {
				if !tt.fail {
					t.Fatalf("3e. [%v] failed to MATCH boolvalue, reason: [%v] vs %v", i, v, tt.boolValue)
				}
			}
		}
	}

	t.Logf("\n** [Test_tomlParseEnvPath][done] {%v} **\n", time.Now().Format(time.RFC3339Nano))
}

func Test_tomlParseLocalConfig(t *testing.T) {
	t.Logf("\n** [Test_tomlParseLocalConfig] {%v} **\n", time.Now().Format(time.RFC3339Nano))
	os.Setenv("conf-location", "./non-exist")
	defer os.Unsetenv("conf-location")

	s, e := config.CreateManager(config.ModelTomlOnly, "conf-location", "./server.conf.env")
	if nil != e {
		t.Fatalf("1. failed to create a tomlmanager, %v", e)
	}
	if e := s.Parse(); nil != e {
		t.Fatalf("2. failed to parse the file contents, %v", e)
	}
	// access values
	type test struct {
		key         string
		value       interface{}
		stringValue string
		intValue    int64
		floatValue  float64
		boolValue   bool
		// 1 = interface, 2 = string, 3 = int, 4 = float32
		expectedValueType int
		fail              bool
	}

	m1 := make(map[string]interface{})
	m1["id"] = 99

	tests := []test{
		{key: "purpose", expectedValueType: 2, stringValue: "testing the toml manager~"},
		{key: "server.ip", expectedValueType: 2, stringValue: "192.168.0.1"},
		{key: "server.port", expectedValueType: 3, intValue: 23891},
		{key: "server.variance", expectedValueType: 4, floatValue: 3.14},
		{key: "server.paths.log", expectedValueType: 2, stringValue: "/var/log/server"},
		{key: "server.paths.debug", expectedValueType: 2, stringValue: "/var/log/server/debug"},
		{key: "obj", expectedValueType: 1, value: m1},
		{key: "flag.run", expectedValueType: 5, boolValue: false},
		{key: "flag.stop", expectedValueType: 5, boolValue: true},

		// wrong type...
		{key: "server.port", fail: true, expectedValueType: 2, stringValue: "1999"},
		// no such key
		{key: "unknown", fail: true},
		// no such key
		{key: "unknown.what", fail: true},
	}

	for i, tt := range tests {
		t.Logf("3. [%v] testing %v", i, tt)
		switch tt.expectedValueType {
		case 1:
			// interface{}
			v, e := s.Value(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3a. [%v] failed to Value, reason: %v", i, e)
				}
			}
			m := v.(map[string]interface{})
			if fmt.Sprintf("%v", m) != fmt.Sprintf("%v", tt.value) {
				if !tt.fail {
					t.Fatalf("3a. [%v] failed to MATCH Value, reason: %v vs %v", i, fmt.Sprintf("%v", m), fmt.Sprintf("%v", tt.value))
				}
			}
		case 2:
			// string
			v, e := s.StringValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3b. [%v] failed to StringValue, reason: %v", i, e)
				}
			}
			if v != tt.stringValue {
				if !tt.fail {
					t.Fatalf("3b. [%v] failed to MATCH StringValue, reason: [%v] vs %v", i, v, tt.stringValue)
				}
			}
		case 3:
			// int
			v, e := s.IntValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3c. [%v] failed to intValue, reason: %v", i, e)
				}
			}
			if v != tt.intValue {
				if !tt.fail {
					t.Fatalf("3c. [%v] failed to MATCH intvalue, reason: [%v] vs %v", i, v, tt.intValue)
				}
			}
		case 4:
			// float32
			v, e := s.FloatValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3d. [%v] failed to floatValue, reason: %v", i, e)
				}
			}
			if v != tt.floatValue {
				if !tt.fail {
					t.Fatalf("3d. [%v] failed to MATCH floatvalue, reason: [%v] vs %v", i, v, tt.floatValue)
				}
			}
		case 5:
			// bool
			v, e := s.BoolValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3e. [%v] failed to boolValue, reason: %v", i, e)
				}
			}
			if v != tt.boolValue {
				if !tt.fail {
					t.Fatalf("3e. [%v] failed to MATCH boolvalue, reason: [%v] vs %v", i, v, tt.boolValue)
				}
			}
		}
	}

	t.Logf("\n** [Test_tomlParseLocalConfig][done] {%v} **\n", time.Now().Format(time.RFC3339Nano))
}

func Test_tomlParseDefaultLocalFile(t *testing.T) {
	t.Logf("\n** [Test_tomlParseDefaultLocalFile] {%v} **\n", time.Now().Format(time.RFC3339Nano))
	os.Setenv("conf-location", "./non-exist")
	defer os.Unsetenv("conf-location")

	s, e := config.CreateManager(config.ModelTomlOnly, "", "", "./server.conf.env")
	if nil != e {
		t.Fatalf("1. failed to create a tomlmanager, %v", e)
	}
	if e := s.Parse(); nil != e {
		t.Fatalf("2. failed to parse the file contents, %v", e)
	}
	// access values
	type test struct {
		key         string
		value       interface{}
		stringValue string
		intValue    int64
		floatValue  float64
		boolValue   bool
		// 1 = interface, 2 = string, 3 = int, 4 = float32
		expectedValueType int
		fail              bool
	}

	m1 := make(map[string]interface{})
	m1["id"] = 99

	tests := []test{
		{key: "purpose", expectedValueType: 2, stringValue: "testing the toml manager~"},
		{key: "server.ip", expectedValueType: 2, stringValue: "192.168.0.1"},
		{key: "server.port", expectedValueType: 3, intValue: 23891},
		{key: "server.variance", expectedValueType: 4, floatValue: 3.14},
		{key: "server.paths.log", expectedValueType: 2, stringValue: "/var/log/server"},
		{key: "server.paths.debug", expectedValueType: 2, stringValue: "/var/log/server/debug"},
		{key: "obj", expectedValueType: 1, value: m1},

		// wrong type...
		{key: "server.port", fail: true, expectedValueType: 2, stringValue: "1999"},
		// no such key
		{key: "unknown", fail: true},
		// no such key
		{key: "unknown.what", fail: true},
	}

	for i, tt := range tests {
		t.Logf("3. [%v] testing %v", i, tt)
		switch tt.expectedValueType {
		case 1:
			// interface{}
			v, e := s.Value(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3a. [%v] failed to Value, reason: %v", i, e)
				}
			}
			m := v.(map[string]interface{})
			if fmt.Sprintf("%v", m) != fmt.Sprintf("%v", tt.value) {
				if !tt.fail {
					t.Fatalf("3a. [%v] failed to MATCH Value, reason: %v vs %v", i, fmt.Sprintf("%v", m), fmt.Sprintf("%v", tt.value))
				}
			}
		case 2:
			// string
			v, e := s.StringValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3b. [%v] failed to StringValue, reason: %v", i, e)
				}
			}
			if v != tt.stringValue {
				if !tt.fail {
					t.Fatalf("3b. [%v] failed to MATCH StringValue, reason: [%v] vs %v", i, v, tt.stringValue)
				}
			}
		case 3:
			// int
			v, e := s.IntValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3c. [%v] failed to intValue, reason: %v", i, e)
				}
			}
			if v != tt.intValue {
				if !tt.fail {
					t.Fatalf("3c. [%v] failed to MATCH intvalue, reason: [%v] vs %v", i, v, tt.intValue)
				}
			}
		case 4:
			// float32
			v, e := s.FloatValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3d. [%v] failed to floatValue, reason: %v", i, e)
				}
			}
			if v != tt.floatValue {
				if !tt.fail {
					t.Fatalf("3d. [%v] failed to MATCH floatvalue, reason: [%v] vs %v", i, v, tt.floatValue)
				}
			}
		case 5:
			// bool
			v, e := s.BoolValue(tt.key)
			if nil != e {
				if !tt.fail {
					t.Fatalf("3e. [%v] failed to boolValue, reason: %v", i, e)
				}
			}
			if v != tt.boolValue {
				if !tt.fail {
					t.Fatalf("3e. [%v] failed to MATCH boolvalue, reason: [%v] vs %v", i, v, tt.boolValue)
				}
			}
		}
	}

	t.Logf("\n** [Test_tomlParseDefaultLocalFile][done] {%v} **\n", time.Now().Format(time.RFC3339Nano))
}
