// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// toml-matic - a TOML supported configuration manager
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package config

import (
	"fmt"
	"io/ioutil"
	"strings"

	t "github.com/pelletier/go-toml"
)

const (
	DelimiterKeyPath string = "."
)

// tomlManager provides the configuration manager capabilities under the TOML syntax.
type tomlManager struct {
	// file contains the configuration content / details for parsing.
	file string
	// fileContent are the configuration content read from the [file] path.
	fileContent []byte

	parsedMap map[string]interface{}
}

// NewTomlManager returns an instance of IManager supporting TOML.
func NewTomlManager(file string) (o IManager, e error) {
	s := new(tomlManager)
	s.file = file

	// load the contents...
	if b, e2 := ioutil.ReadFile(file); nil != e2 {
		e = e2
		return
	} else {
		s.fileContent = b
	}
	s.parsedMap = make(map[string]interface{})

	o = s
	return
}

func (s *tomlManager) Parse(meta ...interface{}) (e error) {
	e = t.Unmarshal(s.fileContent, &s.parsedMap)
	return
}

func (s *tomlManager) Value(key string) (value interface{}, e error) {
	keys := strings.Split(key, DelimiterKeyPath)
	var v interface{}
	// recover() would be trigger for nil reference (e.g. m[k] where m is nil...)
	defer func() {
		if r := recover(); nil != r {
			panic(fmt.Sprintf("[config.toml] unexpected failure to parse keys' value, reason: %v", r))
		}
	}()

	v = s.parsedMap
	for _, k := range keys {
		m, ok := v.(map[string]interface{})
		if !ok {
			e = fmt.Errorf("[config.toml] failed to parse the keys' value: %v", keys)
			return
		}
		v = m[k]
	}
	// finally set the "value" return argument
	value = v
	return
}

func (s *tomlManager) StringValue(key string) (value string, e error) {
	v, e := s.Value(key)
	if nil != e {
		return
	}
	value, ok := v.(string)
	if !ok {
		e = fmt.Errorf("[config.stringValue] failed to convert value to string: %v %v", key, v)
	}
	return
}

func (s *tomlManager) IntValue(key string) (value int64, e error) {
	v, e := s.Value(key)
	if nil != e {
		return
	}
	value, ok := v.(int64)
	if !ok {
		e = fmt.Errorf("[config.intValue] failed to convert value to int: %v %v", key, v)
	}
	return
}

func (s *tomlManager) FloatValue(key string) (value float64, e error) {
	v, e := s.Value(key)
	if nil != e {
		return
	}
	value, ok := v.(float64)
	if !ok {
		e = fmt.Errorf("[config.floatValue] failed to convert value to float32: %v %v", key, v)
	}
	return
}

func (s *tomlManager) BoolValue(key string) (value bool, e error) {
	v, e := s.Value(key)
	if nil != e {
		return
	}
	value, ok := v.(bool)
	if !ok {
		e = fmt.Errorf("[config.boolValue] failed to convert value to bool: %v %v", key, v)
	}
	return
}
