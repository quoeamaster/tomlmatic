// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// toml-matic - a TOML supported configuration manager
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package config

import (
	"fmt"
	"os"
)

const (
	// The default file path holding the configuration contents.
	DefaultConfigFilePath string = "./server.conf"

	// Only TOML is supported.
	ModelTomlOnly int = 1
)

// IManager wraps the common configuration features a config manager component should have.
type IManager interface {
	// Parse is responsible for parsing the configuration source (e.g. from a file or from []byte).
	//
	// The optional "meta" argument is for future extension purpose and provides a way to fit in
	// additional parsing requirements if any.
	Parse(meta ...interface{}) (e error)

	// Returns the value associated with the "key" argument.
	//
	// "key" is dot notated, meaning that "user.name" would first access the "user" object,
	// then traverse to access the "name" property of the configuration.
	Value(key string) (value interface{}, e error)

	// Returns the string value associated with the "key" argument.
	//
	// It is equivalent to:
	// name, ok := manager.Value("user.name").(string)
	StringValue(key string) (value string, e error)

	// Returns the int value associated with the "key" argument.
	//
	// It is equivalent to:
	// name, ok := manager.Value("user.name").(int)
	IntValue(key string) (value int64, e error)

	// Returns the float64 value associated with the "key" argument.
	//
	// It is equivalent to:
	// name, ok := manager.Value("user.name").(float64)
	FloatValue(key string) (value float64, e error)

	// Returns the bool value associated with the "key" argument.
	//
	// It is equivalent to:
	// name, ok := manager.Value("user.name").(bool)
	BoolValue(key string) (value bool, e error)
}

// CreateManager provides a factory method to create the correct [IManager] instance based on the "model" argument.
func CreateManager(model int, configFileEnvName string, localConfigFile string, defaultConfigFile ...string) (o IManager, e error) {
	// config file presence
	// 0. check if the localConfigFile is valid (non "")
	// 1. if 0. fails, check if the env var "configFileEnvName" holds a valid string value
	// 2. if 1. fails, set the file path to the value provided by "defaultConfigFile" or "./server.conf"
	file := localConfigFile
	if "" == file {
		file = os.Getenv(configFileEnvName)
		if "" == file {
			file = DefaultConfigFilePath
			if 0 < len(defaultConfigFile) {
				file = defaultConfigFile[0]
			}
		}
	}
	// [debug]
	//fmt.Printf("[config.create][debug] file chosen: %v\n", file)

	switch model {
	case ModelTomlOnly:
		// create an instance of tomlManager.
		o, e = NewTomlManager(file)
	default:
		e = fmt.Errorf("[config.create] non supported model [%v]", model)
	}
	return
}
